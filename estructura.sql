CREATE TABLE carreras (
  id INT PRIMARY KEY,
  nombre VARCHAR(255) NOT NULL,
  fecalt DATE NOT NULL,
  fecbaj DATE
);


CREATE TABLE materias (
  id INT PRIMARY KEY,
  descripcion VARCHAR(255) NOT NULL,
  nsesio INT NOT NULL,
  durses FLOAT NOT NULL,
  taller VARCHAR(255),
  fecalt DATE NOT NULL,
  fecbaj DATE,
  tipo VARCHAR(255)
);

CREATE TABLE planes (
  id INT PRIMARY KEY,
  carrera INT NOT NULL,
  materia INT NOT NULL,
  fecalt DATE NOT NULL,
  fecbaj DATE,
  area VARCHAR(255),
  reqsim INT,
  requi1 INT,
  requi2 INT,
  requi3 INT,
  requi4 INT,
  semestre INT NOT NULL,
  FOREIGN KEY (carrera) REFERENCES carreras(id),
  FOREIGN KEY (materia) REFERENCES materias(id)
);

