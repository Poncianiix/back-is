const Sequelize = require('sequelize');

const carrerasModel = require('./models/carrera');
const materiasModel = require('./models/materia');
const planesModel = require('./models/plan');

const sequelize = new Sequelize('db1', 'admin', 'abcde12345', {
  host: 'database-1.cabqac5smnov.us-east-1.rds.amazonaws.com',
  dialect: 'mysql'
});

const Carrera = carrerasModel(sequelize, Sequelize);
const Materia = materiasModel(sequelize, Sequelize);
const Plan = planesModel(sequelize, Sequelize);

// Establece relaciones entre las tablas
Carrera.hasMany(Plan, {as: 'planes', foreignKey: 'idCarrera'});
Plan.belongsTo(Carrera, {as: 'carreras', foreignKey: 'idCarrera'});

Materia.hasMany(Plan, {as: 'planes', foreignKey: 'idMateria'});
Plan.belongsTo(Materia, {as: 'materias', foreignKey: 'idMateria'});

sequelize.sync({
  force: false
}).then(() => {
  console.log("Conexión exitosa a la base de datos");
});

module.exports = { Carrera, Materia, Plan };
