const { Materia } = require('../db');

function list(req, res, next) {
    Materia.findAll()
         .then(objects => res.json(objects))
         .catch(error => res.send(error));
}

function index(req, res, next) {
    const id = req.params.id;
    Materia.findByPk(id)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}

function create(req, res, next) {
    const descripcion = req.body.descripcion;
    const nsesio = req.body.nsesio;
    const durses = req.body.durses;
    const taller = req.body.taller;
    const fecalt = req.body.fecalt;
    const fecbaj = req.body.fecbaj;
    const tipo = req.body.tipo;

    let materia = new Object({
        descripcion: descripcion,
        nsesio: nsesio,
        durses: durses,
        taller: taller,
        fecalt: fecalt,
        fecbaj: fecbaj,
        tipo: tipo
    });

    Materia.create(materia)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}

function replace(req, res, next) {
    const id = req.params.id;
    Materia.findByPk(id).then(obj => {
        const descripcion = req.body.descripcion ? req.body.descripcion : "";
        const nsesio = req.body.nsesio ? req.body.nsesio : "";
        const durses = req.body.durses ? req.body.durses : "";
        const taller = req.body.taller ? req.body.taller : "";
        const fecalt = req.body.fecalt ? req.body.fecalt : "";
        const fecbaj = req.body.fecbaj ? req.body.fecbaj : "";
        const tipo = req.body.tipo ? req.body.tipo : "";
        obj.update({
            descripcion: descripcion,
            nsesio: nsesio,
            durses: durses,
            taller: taller,
            fecalt: fecalt,
            fecbaj: fecbaj,
            tipo: tipo
        }).then(Materia => res.json(Materia))
          .catch(err => res.send(err));
    }).catch(err => res.send(err));
}

function update(req, res, next) {
    const id = req.params.id;
    Materia.findByPk(id).then(obj => {
        const descripcion = req.body.descripcion ? req.body.descripcion : obj.descripcion;
        const nsesio = req.body.nsesio ? req.body.nsesio : obj.nsesio;
        const durses = req.body.durses ? req.body.durses : obj.durses;
        const taller = req.body.taller ? req.body.taller : obj.taller;
        const fecalt = req.body.fecalt ? req.body.fecalt : obj.fecalt;
        const fecbaj = req.body.fecbaj ? req.body.fecbaj : obj.fecbaj;
        const tipo = req.body.tipo ? req.body.tipo : obj.tipo;
        obj.update({
            descripcion: descripcion,
            nsesio: nsesio,
            durses: durses,
            taller: taller,
            fecalt: fecalt,
            fecbaj: fecbaj,
            tipo: tipo
        }).then(Materia => res.json(Materia))
          .catch(err => res.send(err));
    }).catch(err => res.send(err));
}


function destroy(req, res, next) {
    const id = req.params.id;
    Materia.destroy({ where: { idMateria: id } })
      .then(obj => res.json(obj))
      .catch(err => res.send(err));
  }

module.exports = { list, index, create, replace, update, destroy };
