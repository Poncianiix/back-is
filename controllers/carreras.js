const { Carrera } = require('../db');

function list(req, res, next) {
  Carrera.findAll()
    .then(objects => res.json(objects))
    .catch(error => res.send(error));
}

function index(req, res, next) {
  const id = req.params.id;
  Carrera.findByPk(id)
    .then(obj => res.json(obj))
    .catch(err => res.send(err));
}

function create(req, res, next) {
  const nombre = req.body.nombre;
  const fecalt = req.body.fecalt;
  const fecbaj = req.body.fecbaj;

  let carrera = {
    nombre: nombre,
    fecalt: fecalt,
    fecbaj: fecbaj
  };

  Carrera.create(carrera)
    .then(obj => res.json(obj))
    .catch(err => res.send(err));
}

function replace(req, res, next) {
  const id = req.params.id;
  Carrera.findByPk(id).then(obj => {
    const nombre = req.body.nombre ? req.body.nombre : "";
    const fecalt = req.body.fecalt ? req.body.fecalt : "";
    const fecbaj = req.body.fecbaj ? req.body.fecbaj : null;

    obj.update({
      nombre: nombre,
      fecalt: fecalt,
      fecbaj: fecbaj
    }).then(Carrera => res.json(Carrera))
      .catch(err => res.send(err));
  }).catch(err => res.send(err));
}

function update(req, res, next) {
  const id = req.params.id;
  Carrera.findByPk(id).then(obj => {
    const nombre = req.body.nombre ? req.body.nombre : obj.nombre;
    const fecalt = req.body.fecalt ? req.body.fecalt : obj.fecalt;
    const fecbaj = req.body.fecbaj ? req.body.fecbaj : obj.fecbaj;

    obj.update({
      nombre: nombre,
      fecalt: fecalt,
      fecbaj: fecbaj
    }).then(Carrera => res.json(Carrera))
      .catch(err => res.send(err));
  }).catch(err => res.send(err));
}

function destroy(req, res, next) {
  const id = req.params.id;
  Carrera.destroy({ where: { idCarrera: id } })
    .then(obj => res.json(obj))
    .catch(err => res.send(err));
}

module.exports = { list, index, create, replace, update, destroy };
