const { Plan } = require('../db');

function list(req, res, next) {
    Plan.findAll({
        include: ["materias", "carreras"]
    })
         .then(objects => res.json(objects))
         .catch(error => res.send(error));
}

function index(req, res, next) {
    const id = req.params.id;
    Plan.findByPk(id)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}

function create(req, res, next) {
    const clave = req.body.clave;
    const idCarrera = req.body.idCarrera;
    const idMateria = req.body.idMateria;
    const fecalt = req.body.fecalt;
    const fecbaj = req.body.fecbaj;
    const area = req.body.area;
    const reqsim = req.body.reqsim;
    const requi1 = req.body.requi1;
    const requi2 = req.body.requi2;
    const requi3 = req.body.requi3;
    const requi4 = req.body.requi4;
    const semestre = req.body.semestre;



    let plan = new Object({
        clave: clave,
        idCarrera: idCarrera,
        idMateria: idMateria,
        fecalt: fecalt,
        fecbaj: fecbaj,
        area: area,
        reqsim: reqsim,
        requi1: requi1,
        requi2: requi2,
        requi3: requi3,
        requi4: requi4,
        semestre: semestre

    });

    Plan.create(plan)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}

function replace(req, res, next) {
    const id = req.params.id;
    Plan.findByPk(id).then(obj => {
        const clave = req.body.clave ? req.body.clave : "";
        const idCarrera = req.body.idCarrera ? req.body.idCarrera : "";
        const idMateria = req.body.idMateria ? req.body.idMateria : "";
        const fecalt = req.body.fecalt ? req.body.fecalt : "";
        const fecbaj = req.body.fecbaj ? req.body.fecbaj : "";
        const area = req.body.area ? req.body.area : "";
        const reqsim = req.body.reqsim ? req.body.reqsim : "";
        const requi1 = req.body.requi1 ? req.body.requi1 : "";
        const requi2 = req.body.requi2 ? req.body.requi2 : "";
        const requi3 = req.body.requi3 ? req.body.requi3 : "";
        const requi4 = req.body.requi4 ? req.body.requi4 : "";
        const semestre = req.body.semestre ? req.body.semestre : "";

        obj.update({
            clave: clave,
            idCarrera: idCarrera,
            idMateria: idMateria,
            fecalt: fecalt,
            fecbaj: fecbaj,
            area: area,
            reqsim: reqsim,
            requi1: requi1,
            requi2: requi2,
            requi3: requi3,
            requi4: requi4,
            semestre: semestre
        }).then(Plan => res.json(Plan))
          .catch(err => res.send(err));
    }).catch(err => res.send(err));
}

function update(req, res, next) {
    const id = req.params.id;
    Plan.findByPk(id).then(obj => {
        const clave = req.body.clave ? req.body.clave : obj.clave;
        const idCarrera = req.body.idCarrera ? req.body.idCarrera : obj.idCarrera;
        const idMateria = req.body.idMateria ? req.body.idMateria : obj.idMateria;
        const fecalt = req.body.fecalt ? req.body.fecalt : obj.fecalt;
        const fecbaj = req.body.fecbaj ? req.body.fecbaj : obj.fecbaj;
        const area = req.body.area ? req.body.area : obj.area;
        const reqsim = req.body.reqsim ? req.body.reqsim : obj.reqsim;
        const requi1 = req.body.requi1 ? req.body.requi1 : obj.requi1;
        const requi2 = req.body.requi2 ? req.body.requi2 : obj.requi2;
        const requi3 = req.body.requi3 ? req.body.requi3 : obj.requi3;
        const requi4 = req.body.requi4 ? req.body.requi4 : obj.requi4;
        const semestre = req.body.semestre ? req.body.semestre : obj.semestre;

        obj.update({
            clave: clave,
            idCarrera: idCarrera,
            idMateria: idMateria,
            fecalt: fecalt,
            fecbaj: fecbaj,
            area: area,
            reqsim: reqsim,
            requi1: requi1,
            requi2: requi2,
            requi3: requi3,
            requi4: requi4,
            semestre: semestre
        }).then(Plan => res.json(Plan))
          .catch(err => res.send(err));
    }).catch(err => res.send(err));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Plan.findByPk(id).then(obj => {
        obj.destroy()
          .then(Plan => res.json(Plan))
          .catch(err => res.send(err));
    }).catch(err => res.send(err));
}

module.exports = { list, index, create, replace, update, destroy };
