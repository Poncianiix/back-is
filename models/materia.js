module.exports = (sequelize, type) => {
const Materia = sequelize.define('materias', {
    idMateria: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    descripcion:type.STRING,
    nsesio: type.INTEGER,     
    durses:type.FLOAT,
    taller: type.STRING,
    fecalt: type.DATEONLY,
    fecbaj:  type.DATEONLY,
    tipo:type.STRING,
  });

  return Materia;
};