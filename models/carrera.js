module.exports = (sequelize, type) => {
    const Carrera = sequelize.define('carreras', {
      idCarrera: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true

      },
      nombre: type.STRING,
      fecalt:type.DATEONLY,
      fecbaj: type.DATEONLY,
    });
    return Carrera;
  };
  