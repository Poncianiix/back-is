module.exports = (sequelize, type) => {
const Plan = sequelize.define('planes', {
    clave: {
      type: type.INTEGER,
      allowNull: false,

    },

    fecalt:  type.DATEONLY,
    fecbaj: type.DATEONLY,
    area: type.STRING,
    reqsim: type.INTEGER,
    requi1:  type.INTEGER,
    requi2:  type.INTEGER,
    requi3:  type.INTEGER,
    requi4: type.INTEGER,
    semestre:type.INTEGER,
    
  });

    return Plan;
};